import java.io.*; 

public class Main {

   public static void main(String[] args) throws IOException
   {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      int i;
      while(true){
         String s = br.readLine();
         i = Integer.parseInt(s);
         if(i != 42)
          System.out.println(i);
         else
          break;
      }

   }

}