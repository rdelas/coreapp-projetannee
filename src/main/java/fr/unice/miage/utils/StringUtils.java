package fr.unice.miage.utils;

public class StringUtils {

	public static boolean isEmpty(String str) {
		return (str == null) ? true : str.trim().isEmpty();
	}

}
