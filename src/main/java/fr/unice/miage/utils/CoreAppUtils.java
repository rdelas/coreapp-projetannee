package fr.unice.miage.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.MultipartFile;

import fr.unice.miage.judge.CoreJudge;
import fr.unice.miage.judge.DockerJudge;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class CoreAppUtils {
	
	private static final Log LOG = LogFactory.getLog(CoreAppUtils.class);
	
	private static final String UNZIP_OUTPUT_DIR = System.getProperty("user.dir")+File.separator+"unzipOutputPath";
	private static final String SAVE_OUTPUT_DIR = System.getProperty("user.dir")+File.separator+"saveOutputPath";

	static {
		File dir = new File(UNZIP_OUTPUT_DIR);
		File dir1 = new File(SAVE_OUTPUT_DIR);
		if(!dir1.exists()) {
			dir1.mkdirs();
		}
		if(!dir.exists()) {
			dir.mkdirs();
		}
	}
	
	/**
	 * 
	 * @param multipartFile
	 * @return
	 */
	public static File saveMultipartFile(String ref, MultipartFile multipartFile) {
		File dir = new File(SAVE_OUTPUT_DIR + File.separator + ref);
		if(!dir.exists()) {
			dir.mkdir();
		}
		
		File saved_file = new File(SAVE_OUTPUT_DIR + File.separator + multipartFile.getOriginalFilename());
		
		try {
			multipartFile.transferTo(saved_file);
		} catch (IllegalStateException | IOException e) {
			LOG.error("Error while saving file.", e);
			return null;
		}
		return saved_file;
	}
	
	/**
	 * 
	 * @param base
	 * @param toFind
	 * @return
	 */
	public static File findFile(File base, final String toFind) {
		File[] files = base.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.contains(toFind);
			}
		});
		return files.length > 0 ? files[0] : null;
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 */
	public static File unZipFile(File file, String ref) {
		File dest = null;
		try {
			ZipFile zipFile = new ZipFile(file.getAbsolutePath());
			zipFile.extractAll(UNZIP_OUTPUT_DIR + File.separator + file.getName().replace(".zip", ""));
			dest = new File(UNZIP_OUTPUT_DIR + File.separator + file.getName().replace(".zip", ""));
		} catch (ZipException e) {
			LOG.error("Error while extracting zip file.", e);
			
		} finally{
			if(dest != null && dest.exists()){
				file.delete();
			}
		}
		
		return dest;
		
	}
	
	/**
	 * 
	 * @param file
	 * @param tests
	 * @return
	 */
	public static boolean isRunnable( MultipartFile file) {
		return !file.isEmpty();
	}
	
	/**
	 * 
	 * @param ref
	 * @param file
	 * @param tests
	 * @param language
	 * @param i
	 * @return
	 * @throws IOException 
	 */
	public static boolean run(String ref, File folderFile, String language, int i, URI callback, List<String> tests) {
		boolean res = true;
		CoreJudge coreJudge = new CoreJudge(ref, folderFile, language, i, callback, tests, new DockerJudge());
		ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.execute(coreJudge);
		return res;
	}
}
