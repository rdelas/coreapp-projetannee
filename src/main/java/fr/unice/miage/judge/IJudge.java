package fr.unice.miage.judge;

import java.io.File;

public interface IJudge {

	public void runJudge(final File workingDirectory, final String language, final int timeout, final String testCaseIds);
	
}
