package fr.unice.miage.judge;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.unice.miage.master.docker.DockerManager;

public class DockerJudge implements IJudge {
	
	private static final Log LOG = LogFactory.getLog(DockerJudge.class);
	
	private final DockerManager dm = DockerManager.getInstance();

	@Override
	public void runJudge(final File workingDirectory, final String language, final int timeout, final String testCaseIds) {
		LOG.info("Copying file to the docker container");
		dm.copyFileToContainer(workingDirectory.getAbsolutePath().replace('\\', '/'), "/root");

		LOG.info("Running judge in the docker container");
		dm.runJudge(language, timeout, testCaseIds, workingDirectory);
		
		LOG.info("Copying results files from the docker container");
		dm.copyFileFromContainer("/root/"+workingDirectory.getName(), workingDirectory.getParentFile().getAbsolutePath().replace('\\', '/'));
		
		LOG.info("Deleting files from the docker container");
		dm.deleteRepositoryOnContainer(workingDirectory);
	}

}
