package fr.unice.miage.judge;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.common.io.Files;

import fr.unice.miage.dto.SubmitJobResultDTO;
import fr.unice.miage.utils.CoreAppUtils;

public class CoreJudge implements Runnable {

	private static final Log LOG = LogFactory.getLog(CoreJudge.class);
	
	private static final String RESPONSE_CALLBACK_PATH = System.getProperty("user.dir")+File.separator+"callback-response";
	
	/**
	 * Unique reference
	 */
	private String ref;

	/**
	 * Working directory folder
	 */
	private File workingDirectory;

	/**
	 * Language
	 */
	private String language;

	/**
	 * Timeout
	 */
	private int timeout;

	/*
	 * Callback
	 */
	private URI callback;
	
	/**
	 * 
	 */
	private List<String> testCaseIds;

	private IJudge judge;
	
	static {
		File f = new File(RESPONSE_CALLBACK_PATH);
		if(!f.exists()){
			f.mkdirs();
		}
	}
	
	/**
	 * 
	 * @param ref
	 * @param workingDirectory
	 * @param language
	 * @param i
	 * @throws IOException
	 */
	public CoreJudge(String ref, File workingDirectory, String language, int timeout, URI callback, List<String> testCaseIds, IJudge judge) {
		this.ref = ref;
		this.workingDirectory = workingDirectory;
		this.language = language;
		this.timeout = timeout;
		this.callback = callback;
		this.testCaseIds = testCaseIds;
		this.judge = judge;
	}

	@Override
	public void run() {
		LOG.info("Running CoreJudge. Ref : "+ref);
		runJudge();
		SubmitJobResultDTO results = getResults();
		RestTemplate rt = new RestTemplate();
		LOG.info("Request send on : " + callback.toString()+". Ref : "+ref);
		try{
			rt.postForObject(callback, results, String.class);
		} catch (HttpClientErrorException e ){
			LOG.error("Client error while sending results on the callback. Ref : "+ref+
					"\nStatus code : "+e.getStatusCode()+
					"\nStatus text : "+e.getStatusText()+
					"\nResponse headers : "+e.getResponseHeaders()
					, e);
			try (FileOutputStream fos = new FileOutputStream(new File(RESPONSE_CALLBACK_PATH + File.separator + "client-"+ ref+".html"))){
				fos.write(e.getResponseBodyAsByteArray());
			} catch (IOException e1) {
				LOG.error("Error while writing log error", e);
			}
		} catch (HttpServerErrorException e){
			LOG.error("Server error while sending results on the callback. Ref : "+ref+
					"\nStatus code : "+e.getStatusCode()+
					"\nStatus text : "+e.getStatusText()+
					"\nResponse headers : "+e.getResponseHeaders()
					, e);
			try (FileOutputStream fos = new FileOutputStream(new File(RESPONSE_CALLBACK_PATH + File.separator + "server-"+ ref+".html"))){
				fos.write(e.getResponseBodyAsByteArray());
			} catch (IOException e1) {
				LOG.error("Error while writing log error", e);
			}
		} catch (RestClientException e){
			LOG.error("Unknown while sending results on the callback. Ref : "+ref, e);
		}
		LOG.info("End running CoreJudge. Ref : "+ref);
	}

	private SubmitJobResultDTO getResults() {
		LOG.info("Getting results for the run "+ref);
		SubmitJobResultDTO dto = new SubmitJobResultDTO();
		dto.setRef(ref);
		
		File buildFile = CoreAppUtils.findFile(workingDirectory, "JUDGE_BUILD.txt");
		File buildStatusFile = CoreAppUtils.findFile(workingDirectory,"JUDGE_BUILD_STATUS.txt");
		File judgeLogsFile = CoreAppUtils.findFile(workingDirectory, "JUDGE_LOGS.txt");

		if (buildFile != null) {
			try {
				dto.setBuildResult(new String(Files.toByteArray(buildFile)));
			} catch (IOException e) {
				LOG.error("Error while getting build result", e);
				dto.setBuildResult("Error while getting build result");
			}
		} else {
			dto.setBuildResult("No build");
		}
		
		if (buildStatusFile != null) {
			try {
				dto.setBuildVerdict(new String(Files.toByteArray(buildStatusFile)));
			} catch (IOException e) {
				LOG.error("Error while getting build verdict", e);
				dto.setBuildVerdict("Error while getting build verdict");
			}
		} else {
			dto.setBuildVerdict("No build");
		}
		
		if (judgeLogsFile != null) {
			try {
				LOG.info("Judge Logs : \n" + new String(Files.toByteArray(judgeLogsFile)));
			} catch (IOException e) {
				LOG.error("Error while judge logs founds", e);
			}
		} else {
			LOG.warn("No judge logs founds");
		}
		
		if(!this.language.equals("maven")) {
			File[] testCases = workingDirectory.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File arg0, String arg1) {				
					for(String s : testCaseIds){
						if(s.equals(arg1)) {
							return true;
						}
					}
					return false;
				}
			});
			for (File testCase : testCases) {
				File responsesFile = CoreAppUtils.findFile(testCase, "JUDGE_REPONSE.txt");
				File verdictsFile = CoreAppUtils.findFile(testCase,"JUDGE_VERDICT.txt");
	
				if (verdictsFile != null) {
					try {
						dto.putResult(testCase.getName(),
								new String(Files.toByteArray(verdictsFile)));
					} catch (IOException e) {
						LOG.error("Error while getting verdict for test case " + testCase.getName(), e);
						dto.putResult(testCase.getName(), "Error while getting verdict for test case "+testCase.getName());
					}
				} else {
					dto.putResult(testCase.getName(), "404");
				}
				if (responsesFile != null) {
					try {
						dto.putRunResult(testCase.getName(),
								new String(Files.toByteArray(responsesFile)));
					} catch (IOException e) {
	
						LOG.error("Error while getting verdict for test case " + testCase.getName(), e);
						dto.putRunResult(testCase.getName(), "Error while getting response for test case "+testCase.getName());
					}
				} else {
					dto.putRunResult(testCase.getName(), "No reponse");
				}
			}
		}
		else {
			File mavenResultFile = CoreAppUtils.findFile(workingDirectory, "MAVEN_RESULTS.txt");
			if(mavenResultFile != null) {
				try {
					String res = new String(Files.toByteArray(mavenResultFile));
					if(!res.isEmpty()) {
						dto.putRunResult("1", res);
						dto.putResult("1", res.contains("Failures: 0, Errors: 0") ? "200" : "401");
					}
					
				} catch (IOException e) {
					LOG.error("Error while getting maven results file", e);
				}
			}
		}
		
		return dto;
	}

	/**
	 * 
	 * @param fileToExecute
	 * @param input
	 * @param output
	 * @return
	 * @throws IOException
	 */
	private void runJudge() {
		LOG.info("CoreJudge.runJudge()");
		judge.runJudge(workingDirectory, language, timeout, getTestCases());
		LOG.info("CoreJudge.runJudge() end");
		
//		Process p;
//
//		String judgeCmd = PYTHON_PATH + " \"" + JUDGE_HOME + File.separator + JUDGE_FILE_NAME + "\""
//				+ " -l " + language
//				+ " -t " + timeout
//				+ " -c " + getTestCases()
//				+ " -d" 
//				+ " \"" + workingDirectory.getAbsolutePath().replace('\\', '/') +"\"";
//
//		System.out.println(judgeCmd);
//		StringBuilder builder = new StringBuilder();
//		try {
//			p = Runtime.getRuntime().exec(judgeCmd);
//			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
//			String line = "";
//	
//			while ((line = br.readLine()) != null) {
//				builder.append(line);
//				System.out.println(line);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	private String getTestCases() {
		LOG.info("Recovering test cases. Ref : "+ref);
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < this.testCaseIds.size(); i++) {
			if(i != 0) {
				builder.append(',');
			}
			builder.append(this.testCaseIds.get(i));
		}
		return builder.toString();
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public File getWorkingDirectory() {
		return workingDirectory;
	}

	public void setWorkingDirectory(File folderSources) {
		this.workingDirectory = folderSources;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public URI getCallback() {
		return callback;
	}

	public void setCallback(URI callback) {
		this.callback = callback;
	}

}
