package fr.unice.miage.web.controllers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.unice.miage.dto.SubmitJobQueryDTO;
import fr.unice.miage.utils.ClassUtil;
import fr.unice.miage.utils.CoreAppUtils;

@RestController
@RequestMapping(path = "/CoreJudge")
public class CoreJudgeController {

	private static final Log LOG = LogFactory.getLog(CoreJudgeController.class);

	@RequestMapping(path = "/", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public @ResponseBody String ping(HttpServletRequest request) {
		LOG.info("/CoreJudge/");
		LOG.info("Request recieved from " + request.getRemoteHost());
		String response = "<!DOCTYPE html>" 
				+ "<html lang=\"fr\">" 
				+ "<head>" 
				+ "<meta charset=\"utf-8\">"
				+ "<title>CoreAppJudge - Hello World</title>" 
				+ "</head>" 
				+ "<body>"
				+ "<h1>Hello from Core App Judge</h1>" 
				+ "<p>" 
				+ "Core App Judge is running" 
				+ "</p>"
				+ "</body></html>";

		return response;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/submitJob", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody HashMap<String, String> submit(HttpServletRequest request, SubmitJobQueryDTO query) {
		LOG.debug("/CoreJudge/submitJob");
		LOG.info("Request recieved from " + request.getRemoteHost());
		HashMap<String, String> response = new HashMap<String, String>();

		if (validate(query, response)) {
			File savedFile;
			if ((savedFile = CoreAppUtils.saveMultipartFile(query.getRef(), query.getFile())) != null) {
				File unzipFile;
				if ((unzipFile = CoreAppUtils.unZipFile(savedFile, query.getRef())) != null) {

					CoreAppUtils.run(query.getRef(), unzipFile, query.getLanguage(), query.getTimeout(),
							query.getCallbackAsURI(), query.getTestCaseIdsArray());
					response.put("code", "200");
					response.put("message", "ok");

				} else {
					LOG.error("Error while unzipping file");
					response.put("code", "500");
					response.put("message", "Le fichier n'a pas pu être dézippé.");
				}
			} else {
				LOG.error("Error while saving file");
				response.put("code", "500");
				response.put("message", "Le fichier n'a pas pu être enregistré.");
			}
		}
		LOG.info(ClassUtil.toString(response));
		return response;
	}

	private boolean validate(SubmitJobQueryDTO query, HashMap<String, String> response) {
		LOG.info("Validation of the query");

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<SubmitJobQueryDTO>> constraintViolations = validator.validate(query);
		boolean isValid = true;

		if (!constraintViolations.isEmpty()) {

			LOG.error("The query is not valid");
			isValid = false;
			for (ConstraintViolation<SubmitJobQueryDTO> contraintes : constraintViolations) {
				response.put(contraintes.getPropertyPath().toString(), contraintes.getRootBeanClass().getSimpleName()
						+ "." + contraintes.getPropertyPath() + " " + contraintes.getMessage());
			}
		}

		return isValid;
	}
}
