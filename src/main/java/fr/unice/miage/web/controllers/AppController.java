package fr.unice.miage.web.controllers;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.unice.miage.master.docker.DockerManager;

@RestController
@RequestMapping(path = "/manage")
public class AppController {
	
	private static final Log LOG = LogFactory.getLog(AppController.class);
	
	private final DockerManager dockerManager = DockerManager.getInstance();
	
	@RequestMapping(path="/status", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody HashMap<String, String> coreStatus(HttpServletRequest request) {
		LOG.info("Asking for Docker container status from "+request.getRemoteHost());
		HashMap<String, String> response = new HashMap<>();
		
		String status = dockerManager.getContainerStatus();
		LOG.info("Container status : "+status);
		response.put("status", status);
        return response;
    }
	
	@RequestMapping(path="/restart", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody HashMap<String, Object> restartCore(HttpServletRequest request) {
		LOG.info("Asking for restarting Docker container from "+request.getRemoteHost());
		HashMap<String, Object> response = new HashMap<>();
		
		boolean res = dockerManager.restartContainer();
		response.put("exec", res);
		String status = dockerManager.getContainerStatus();
		LOG.info("Container status after restart : "+status);
		response.put("status", status);
        return response;
    }
	
	@RequestMapping(path="/buildrestart", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody HashMap<String, Object> restartCoreWithBuild(HttpServletRequest request) {
		LOG.info("Asking for building and restarting Docker container from "+request.getRemoteHost());
		HashMap<String, Object> response = new HashMap<>();
		
		boolean res = dockerManager.restartContainerWithRebuild();
		response.put("exec", res);
		String status = dockerManager.getContainerStatus();
		LOG.info("Container status after restart with build : "+status);
		response.put("status", status);
        return response;
    }
	
	@RequestMapping(path="/start", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody HashMap<String, Object> startCore(HttpServletRequest request) {
		LOG.info("Asking for starting Docker container from "+request.getRemoteHost());
		HashMap<String, Object> response = new HashMap<>();
		
		boolean res = dockerManager.startContainer();
		response.put("exec", res);
		String status = dockerManager.getContainerStatus();
		LOG.info("Container status after start : "+status);
		response.put("status", status);
        return response;
    }
	
	@RequestMapping(path="/buildstart", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody HashMap<String, Object> buildAndStartCore(HttpServletRequest request) {
		LOG.info("Asking for building and starting Docker container from "+request.getRemoteHost());
		HashMap<String, Object> response = new HashMap<>();
		
		boolean res = dockerManager.buildAndStartContainer();
		response.put("exec", res);
		String status = dockerManager.getContainerStatus();
		LOG.info("Container status after build and start : "+status);
		response.put("status", status);
        return response;
    }
	
	
	@RequestMapping(path="/stop", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody HashMap<String, Object> stopCore(HttpServletRequest request) {
		LOG.info("Asking for stopping Docker container from "+request.getRemoteHost());
		HashMap<String, Object> response = new HashMap<>();
		
		boolean res = dockerManager.stopContainer();
		response.put("exec", res);
		String status = dockerManager.getContainerStatus();
		LOG.info("Container status after stop : "+status);
		response.put("status", status);
        return response;
    }
}
