package fr.unice.miage.dto;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.json.JSONArray;
import org.springframework.web.multipart.MultipartFile;

public class SubmitJobQueryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7947602026706247197L;
	
	@NotNull
	@Size(min=1)
	private String ref;
	
	@Min(value=1L)
	private int timeout;
	
	@NotNull
	@Size(min=1)
	private String language;
	
	@NotNull
	private String testCaseIds;
	
	@NotNull
	private MultipartFile file;
	
	@NotNull
	private URL callback;

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTestCaseIds() {
		return testCaseIds;
	}
	
	public List<String> getTestCaseIdsArray() {
		List<String> list = new ArrayList<>();
		JSONArray json = new JSONArray(testCaseIds);
		for(int i = 0; i< json.length(); i++) {
			list.add(String.valueOf(json.get(i)));
		}
		return list;
	}
	
	public void setTestCaseIds(String testCaseIds) {
		this.testCaseIds = testCaseIds;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public URL getCallback() {
		return callback;
	}

	public void setCallback(URL callback) {
		this.callback = callback;
	}

	public URI getCallbackAsURI() {
		URI uri = null;
		try {
			uri = callback.toURI();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			System.err.println(e);
		}
		
		return uri;
	}
}
