package fr.unice.miage.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonView;

public class SubmitJobResultDTO implements Serializable {

	private static final long serialVersionUID = -4602077389119628203L;
	
	@JsonView()
	private Map<String, String> runResults;
	
	@JsonView()
	private String buildVerdict;
	
	@JsonView()
	private String buildResult;
	
	@JsonView()
	private Map<String, String> results;
	
	@JsonView
	private String ref;
	
	public SubmitJobResultDTO() {
		this.runResults = new HashMap<>();
		this.results = new HashMap<>();
	}
	
	public void setBuildVerdict(String buildVerdict) {
		this.buildVerdict = buildVerdict;
	}
	
	public String getBuildVerdict() {
		return buildVerdict;
	}
	
	public void putResult(String key, String value){
		this.results.put(key, value);
	}
	
	public void putAllResults(Map<String, String> results){
		this.results.putAll(results);
	}
	
	public Map<String, String> getResults() {
		return results;
	}
	
	public void putRunResult(String key, String value){
		this.runResults.put(key, value);
	}
	
	public void putAllRunResults(Map<String, String> results){
		this.runResults.putAll(results);
	}
	
	public Map<String, String> getRunResults() {
		return runResults;
	}
	

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}


	public String getBuildResult() {
		return buildResult;
	}


	public void setBuildResult(String buildResult) {
		this.buildResult = buildResult;
	}
}
