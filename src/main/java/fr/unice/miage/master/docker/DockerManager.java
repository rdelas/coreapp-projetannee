package fr.unice.miage.master.docker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.ExecCreateCmdResponse;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.api.model.RestartPolicy;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.BuildImageResultCallback;
import com.github.dockerjava.core.command.ExecStartResultCallback;
import com.github.dockerjava.core.util.CompressArchiveUtil;
import com.github.dockerjava.jaxrs.DockerCmdExecFactoryImpl;

import fr.unice.miage.utils.LoggingOutputStream;
import fr.unice.miage.utils.LoggingOutputStream.Level;
import fr.unice.miage.utils.OSValidator;

public class DockerManager {

	private static final Log LOG = LogFactory.getLog(DockerManager.class);
	
	public static final String IMAGE_NAME = "buffalojudge/coreapp";
	
	public static final String IMAGE_BASE_NAME = "buffalojudge/base";

	public static final String CONTAINER_NAME = "coreapp-container";

	public static final String DOCKERFILE_BASE = File.separator+"dockerfiles"+File.separator+"base";
	
	public static final String DOCKERFILE_COREAPP = File.separator+"/dockerfiles"+File.separator+"coreapp";
	
	public static final String DOCKERHOST = "192.168.99.100:2376";

	private static DockerManager instance;
	private DockerClient dockerClient;
	private DockerCmdExecFactoryImpl dockerCmdExecFactory;
	private DockerClientConfig config;
	private String containerId;

	private DockerManager() {

		LOG.info("#################### Initialazing Docker Manager #########################");
		createConfig();
		createDockerFactory();		
		createDockerClient();
		buildImages();
		stopContainer();
		removeContainer();
		buildContainer();
		startContainer();
		LOG.info("#################### Docker Manager initialized  #########################\n");
	}
	

	public static DockerManager getInstance() {
		if (instance == null) {
			DockerManager.instance = new DockerManager();
		}
		return DockerManager.instance;
	}
	
	private void createConfig(){
		LOG.info("Creation of the Docker API configuration");
		String dockerHost;
		if(OSValidator.isUnix()) {
			dockerHost = "unix:///var/run/docker.sock";
		}
		else {
			dockerHost= "tcp://" + DOCKERHOST;
		}
		
		this.config = DockerClientConfig.createDefaultConfigBuilder()
				.withDockerHost(dockerHost)
				.withDockerTlsVerify(true)
				.withApiVersion("1.21")
				.withDockerConfig(System.getProperty("user.home") + File.separator + ".docker")
				.withDockerCertPath(System.getProperty("user.home") + File.separator + ".docker" + File.separator + "machine" + File.separator + "machines" + File.separator + "default")
				.withRegistryUrl("http://index.docker.io/v1/")
				.build();
	}
	
	@SuppressWarnings("resource")
	private void createDockerFactory() {
		LOG.info("Creation of the Docker API factory");
		this.dockerCmdExecFactory = new DockerCmdExecFactoryImpl()
				.withReadTimeout(0)
				.withConnectTimeout(0)
				.withMaxTotalConnections(100)
				.withMaxPerRouteConnections(10);
	}
	
	private void createDockerClient(){
		LOG.info("Creation of the Docker API client");
		this.dockerClient = DockerClientBuilder.getInstance(config)
				.withDockerCmdExecFactory(dockerCmdExecFactory)
				.build();
	}
	
	private void buildImages(){

		LOG.info("Checking of the docker images");
		List<Image> images = dockerClient.listImagesCmd()
				.withShowAll(true)
				.withImageNameFilter(IMAGE_NAME).exec();
		if (images.isEmpty()) {
			LOG.info("Docker "+IMAGE_NAME+" image doesn't exists");
			List<Image> imagesBase = dockerClient.listImagesCmd()
					.withShowAll(true)
					.withImageNameFilter(IMAGE_BASE_NAME).exec();
			if(imagesBase.isEmpty()){
				LOG.info("Docker "+IMAGE_BASE_NAME+" image doesn't exists");
				LOG.info("Creation of the  "+IMAGE_BASE_NAME+" image");
				dockerClient.buildImageCmd(new File(getClass().getResource(DOCKERFILE_BASE).getFile()))
						.withNoCache(false)
						.withTag(IMAGE_BASE_NAME)
						.exec(new BuildImageResultCallback()).awaitImageId();
			}

			LOG.info("Creation of the  "+IMAGE_NAME+" image");
			dockerClient.buildImageCmd(new File(getClass().getResource(DOCKERFILE_COREAPP).getFile()))
					.withNoCache(false)
					.withTag(IMAGE_NAME)
					.exec(new BuildImageResultCallback()).awaitImageId();
		}
		LOG.info("Docker images OK");
	}
	
	private boolean removeContainer(){
		
		String containerId = (this.containerId == null) ? getContainerID(CONTAINER_NAME) : this.containerId;
		if(containerId == null){
			return false;
		}
		LOG.info("Removing container "+CONTAINER_NAME+". ID = "+containerId);
		try{
			dockerClient.removeContainerCmd(containerId).exec();
		}catch(Exception e){
			LOG.error("Error while removing container. ID = "+containerId, e);
			return false;
		}
		LOG.info("Container "+CONTAINER_NAME+" removed. ID = "+containerId);
		return true;
	}
	
	private String getContainerID(String name){
		List<Container> containers = dockerClient.listContainersCmd().withShowAll(true).exec();
		for (Container container : containers) {
			for(String containerName : container.getNames()){
				if(containerName.contains(name))
					return container.getId();
			}
			
		}
		return null;
	}
	
	private boolean buildContainer(){

		LOG.info("Creation of container "+CONTAINER_NAME+".");
		RestartPolicy restartPolicy = RestartPolicy.onFailureRestart(5);
		try{
			this.containerId = dockerClient.createContainerCmd(IMAGE_NAME)
					.withName(CONTAINER_NAME)
					.withRestartPolicy(restartPolicy)
					.withAttachStdin(true)
					.withStdinOpen(true)
					.withTty(true)
					.exec().getId();
		} catch (Exception e){
			LOG.error("Error while creating container "+CONTAINER_NAME, e);
			return false;
		}
		LOG.info("Container "+CONTAINER_NAME+" created. ID ="+this.containerId);
		return true;
	}
	
	public boolean buildAndStartContainer(){
		LOG.info("Buildind and starting container "+CONTAINER_NAME+".");
		String containerId = (this.containerId == null) ? getContainerID(CONTAINER_NAME) : this.containerId;
		
		InspectContainerResponse icr = dockerClient.inspectContainerCmd(containerId).exec();
		if(icr.getState().getRunning()){
			return false;
		}
		boolean remove = removeContainer();
		boolean create = buildContainer();
		boolean start = startContainer();
		
		boolean res = remove && create && start;
		
		if(res)
			LOG.info("Container "+CONTAINER_NAME+" started. ID ="+this.containerId);
		else
			LOG.error("Error while building and start container. ID ="+this.containerId);
		return res;
	}
	

	public boolean startContainer() {
		String containerId = (this.containerId == null) ? getContainerID(CONTAINER_NAME) : this.containerId;

		InspectContainerResponse icr = dockerClient.inspectContainerCmd(containerId).exec();
		if(icr.getState().getRunning()){
			LOG.warn("Container "+CONTAINER_NAME+" is already running. ID ="+this.containerId);
			return false;
		}
		
		LOG.info("Starting container "+CONTAINER_NAME+". ID ="+this.containerId);
		try{
		dockerClient.startContainerCmd(this.containerId).exec();
		}catch(Exception e){
			LOG.error("Error while starting container. ID = "+containerId, e);
			return false;
		}
		LOG.info("Container "+CONTAINER_NAME+" started. ID ="+this.containerId);
		
		return true;
	}
	
	public boolean stopContainer() {		
		String containerId = (this.containerId == null) ? getContainerID(CONTAINER_NAME) : this.containerId;

		if(containerId == null){
			return false;
		}
		
		InspectContainerResponse icr = dockerClient.inspectContainerCmd(containerId).exec();
		if(!icr.getState().getRunning()){
			LOG.warn("Container "+CONTAINER_NAME+" is already stopped. ID ="+this.containerId);
			return false;
		}
		
		if(containerId != null){
			LOG.info("Stopping container "+CONTAINER_NAME+". ID =c"+containerId);
			try{
				dockerClient.stopContainerCmd(containerId).exec();
			} catch (Exception e){
				LOG.error("Error while stopping container. ID = "+containerId, e);
				return false;
			}
			LOG.info("Container "+CONTAINER_NAME+" stopped. ID =c"+containerId);
		}
		return true;
	}

	
	public boolean restartContainer(){
		String containerId = (this.containerId == null) ? getContainerID(CONTAINER_NAME) : this.containerId;
		
		if(containerId == null){
			return false;
		}
			LOG.info("Restarting container "+CONTAINER_NAME+". ID ="+containerId);
			try{
				dockerClient.restartContainerCmd(containerId).exec();
			} catch (Exception e){
				LOG.error("Error while restarting container: ID = "+containerId, e);
				return false;
			}
			LOG.info("Container "+CONTAINER_NAME+" restarted. ID ="+containerId);
			return true;
		
	}
	
	public boolean restartContainerWithRebuild(){
		LOG.info("Restarting container "+CONTAINER_NAME+" with build. ID ="+this.containerId);
		boolean stop = stopContainer();		
		boolean remove = removeContainer();
		boolean create = buildContainer();
		boolean start = startContainer();
		boolean res = stop && remove && create && start;
		if(res)
			LOG.info("Container "+CONTAINER_NAME+" restarted. ID ="+this.containerId);
		else
			LOG.error("Error while restarting with rebuild container. ID ="+this.containerId);
		
		return res;
	}

	public String getContainerStatus(){
		InspectContainerResponse icr = dockerClient.inspectContainerCmd(containerId).exec();
		String status = icr.getState().getStatus();
		LOG.info("Docker container status : "+status);
		return status;
	}
	
	public void getDockerInfo() {
		Info info = dockerClient.infoCmd().exec();
		LOG.info(info);
	}

	public void copyFileToContainer(String src, String dest) {
		LOG.debug("SRC : "+src);
		LOG.debug("DEST : "+dest);
		Path temp;
		try {
			temp = Files.createTempFile("temp", ".tar.gz");
			CompressArchiveUtil.tar(Paths.get(src), temp, true, false);
			InputStream uploadStream = Files.newInputStream(temp);

			LOG.info("Copying archive "+src+" to the container");
			dockerClient.copyArchiveToContainerCmd(this.containerId)
			.withRemotePath(dest)
			.withTarInputStream(uploadStream)
			.exec();
		} catch (IOException e) {
			LOG.error("Error while copying archive to the container "+CONTAINER_NAME+". ID = "+this.containerId, e);
		}
		
	}

	public void copyFileFromContainer(String src, String dest) {
		LOG.debug("SRC : "+src);
		LOG.debug("DEST : "+dest);

		LOG.info("Copying archive "+src+" from the container");
		InputStream response = dockerClient.copyArchiveFromContainerCmd(containerId, src).exec();
		try {
			Boolean bytesAvailable = response.available() > 0;
			if(!bytesAvailable)
				throw new IOException("La réponse reçue est vide");
			TarArchiveInputStream tarInputStream = new TarArchiveInputStream(response);
			TarArchiveEntry nextTarEntry = null;
			while ((nextTarEntry = tarInputStream.getNextTarEntry()) != null) {
				if (!nextTarEntry.isDirectory()) {
					System.out.println(nextTarEntry.getName());
					int size = (int) nextTarEntry.getSize();
					byte[] read = new byte[size];
					
					tarInputStream.read(read, 0, size);
					

					File out = new File(dest + File.separator + nextTarEntry.getName());
					FileUtils.forceMkdir(out.getParentFile());
					FileOutputStream fos = new FileOutputStream(out);

					IOUtils.write(read, fos);
					fos.close();
				}
			}
			tarInputStream.close();

		} catch (IOException e) {
			LOG.error("Error while copying archive from the container "+CONTAINER_NAME+". ID = "+this.containerId, e);
		}

	}

	public void runJudge(String language, Integer timeout, String testCases, File workingDirectory) {
		String lang = "-l " + language;
		String to = "-t " + timeout;
		String tc = "-c " + testCases;
		String d = "-d /root/" + workingDirectory.getName();
		LOG.info("Running judge into the container : \njudge "+lang+" "+to+" "+tc+" "+d);
		ExecCreateCmdResponse execCreateCmdResponse = dockerClient.execCreateCmd(containerId)
				.withAttachStdout(true)
				.withAttachStderr(true)
				.withCmd("judge",lang, to, tc, d).exec();
		try {
			dockerClient.execStartCmd(execCreateCmdResponse.getId())
						.exec(new ExecStartResultCallback(new LoggingOutputStream(LOG, Level.INFO), 
											  			  new LoggingOutputStream(LOG, Level.ERROR)))
						.awaitCompletion();
		} catch (InterruptedException e) {
			LOG.error("Error while running command : judge "+lang+" "+to+" "+tc+" "+d, e);
		}
	}
	
	public void deleteRepositoryOnContainer(File workingDirectory){

		String param = "/root/" + workingDirectory.getName();
		LOG.info("Removing repository "+workingDirectory.getName()+" into the container : \nrm -R "+param);
		ExecCreateCmdResponse execCreateCmdResponse = dockerClient.execCreateCmd(containerId)
				.withAttachStdout(true)
				.withAttachStderr(true)
				.withCmd("rm","-R",param).exec();

		try {
			dockerClient.execStartCmd(execCreateCmdResponse.getId())
						.exec(new ExecStartResultCallback(new LoggingOutputStream(LOG, Level.INFO), 
													  	  new LoggingOutputStream(LOG, Level.ERROR)))
						.awaitCompletion();
		} catch (InterruptedException e) {
			LOG.error("Error while running command : rm -R "+param, e);
		}
	}

	
}
