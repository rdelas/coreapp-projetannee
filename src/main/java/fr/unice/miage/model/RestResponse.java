package fr.unice.miage.model;

public class RestResponse {

	private Object response;
	
	private Integer code;
	
	private String error;

	public RestResponse() {
		super();
	}

	public RestResponse(Object response, Integer code, String error) {
		this();
		this.response = response;
		this.code = code;
		this.error = error;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
	
}
