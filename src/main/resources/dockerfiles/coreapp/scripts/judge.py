#
#      JUDGE PY
#    Sacha Cochin
#
# Simple python multi-language code judge
#
# |__workingDir     -workingdir
# | |_12314
# | | |_INPUTS
# | | |_OUPUTS
# | |_Main.xxx
# |
#
# The script require g++, gcc and python to run
#

import os, filecmp, sys, getopt, datetime, time, platform, signal, re

TESTS_SEPARATOR = ','

MAKEFILE_ESPECTED_GOAL = 'main'
MAIN_FILE_NAME = "Main"
OUTPUT_FILE_NAME = "OUTPUT.txt"
INPUT_FILE_NAME = "INPUT.txt"

VERDICT_FILE_NAME = "JUDGE_VERDICT.txt"
BUILD_STATUS_NAME = "JUDGE_BUILD_STATUS.txt"
BUILD_FILE_NAME = "JUDGE_BUILD.txt"
LOG_FILE_NAME = "JUDGE_LOGS.txt"
RUN_RESPONSE_FILE_NAME = "JUDGE_REPONSE.txt"

ERR = "ERROR"
LOG = "LOG"
WARN = "WARNING"
INFO = "INFO"
DEBUG = "DEBUG" 

SUPPORTED_LANGUAGES = ('java', 'maven', 'python', 'c', 'cpp', 'make')
COMPILED_LANGUAGES = ("java", "c", "cpp", 'maven', 'make')

CODES = { 200:'success', 404:'file not found', 400:'execution error', 401:'wrong answer', 402:'compile error', 408:'timeout error' }

# Check the working directory
def workingDirCorrect(workingDir, language):
    if language == 'maven':
        mavenFileHere = False
        for f in os.listdir(workingDir):
            if f == 'pom.xml':
                mavenFileHere = True    
        return mavenFileHere
    elif language == 'make':
        makefileFileHere = False
        for f in os.listdir(workingDir):
            if f == 'Makefile':
                makefileFileHere = True    
        return makefileFileHere
    else:
        return True
        

def getTestCases(workingDir, tests):
    testCases = []
    for f in os.listdir(workingDir):
        for test in tests:
            if f == test:
                testCases.append(f)
    return testCases

# Simple logger
class MyLogger:
    def __init__(self, workingDir, verbose):
        self.verbose = verbose
        self.logfile = workingDir + os.sep + LOG_FILE_NAME
        
    def log(self, type, message):
        log = type + " : " + message + os.linesep
        if(self.verbose):
            print(log)
        with open(self.logfile, "a") as myfile:
           myfile.write(log)

class Compiler:
    def __init__(self,workingDir, language, includes, logger):
        self.logger = logger
        self.workingDirectory = workingDir
        self.language = language
        self.includes = includes
        self.buildLogFile = workingDir + os.sep + BUILD_FILE_NAME
        self.submitedFile = workingDir + os.sep + MAIN_FILE_NAME
    
    def getSourceFileExtension(self):
        if self.language == "java":
            return ".java"
        elif self.language == "cpp":
            return ".cpp"
        elif self.language == "c":
            return ".c"
        elif self.language == "python":
            return ".py"
        else:
            return ""

    def getExecutionFileExtension(self):
        if self.language == "java":
            return ".java"
        elif self.language in ("cpp", "c", "make"):
            return ".out"
        elif self.language == "python":
            return ".py"

    def isCompiled(self):
        compiled = False
        fileToFind = MAIN_FILE_NAME + self.getExecutionFileExtension()
        self.logger.log(INFO, "Searching for " + fileToFind)

        for file in os.listdir(self.workingDirectory):
            if file == fileToFind:
                compiled = True 
        return compiled
    
    def mainSourceFileExists(self):
        mainFileHere = False
        for f in os.listdir(self.workingDirectory):
            if MAIN_FILE_NAME + self.getSourceFileExtension() == f:
                mainFileHere = True
        return mainFileHere
    
    def compileSources(self):
        
        if self.language not in ("maven", "make") and not self.mainSourceFileExists():
            self.logger.log(ERR, "No '" + str(self.submitedFile) + self.getSourceFileExtension() + "' file to compile found")
            return 404
        
        # Build the compile command line
        compileCmd = ''
        if self.language == 'java':
            compileCmd = 'javac -verbose ' + str(self.submitedFile) + self.getSourceFileExtension() 
        elif self.language == 'maven':
            compileCmd = 'mvn clean test -f ' + self.workingDirectory + os.sep + 'pom.xml'
        elif self.language == 'c':
            compileCmd = 'gcc ' + str(self.submitedFile) + self.getSourceFileExtension()
            if self.includes != '':
                compileCmd = compileCmd + ' -l' + self.includes
            compileCmd = compileCmd + " -o " + self.submitedFile + ".out"
        elif self.language == 'cpp':
            if self.includes != '':
                compileCmd = compileCmd + ' -l' + self.includes
            compileCmd = 'g++ ' + str(self.submitedFile) + self.getSourceFileExtension() + " -o " + self.submitedFile + ".out"
        elif self.language == 'make':
            compileCmd = 'make ' + MAKEFILE_ESPECTED_GOAL + ' -C ' + self.workingDirectory
        
        compileCmd = compileCmd + " > " + self.buildLogFile
        
        self.logger.log(INFO, "Compiling sources with command " + compileCmd)
        
        # Running the command line
        os.system(compileCmd)
        self.logger.log(INFO, "Compile finished")

        status = 200
        #Return status code
        if self.language == 'maven':
            self.logger.log(INFO, 'Parsing Maven results')
            if os.path.isfile(self.buildLogFile):
                ff = open(self.buildLogFile, "r")
                for line in ff :
                    if re.search('BUILD SUCCESS', line):
                        self.logger.log(INFO, "Maven build succeed")
                        status = 200
                    elif re.search(r'Tests run:(.*)Time elapsed:', line):
                        testsResults = line.split(", T")[0]
                        os.system('echo "' + testsResults + '" > ' + self.workingDirectory + os.sep + 'MAVEN_RESULTS.txt')
                        self.logger.log(INFO, "Tests passed : " + testsResults)
                    elif re.search('BUILD FAILURE', line):
                        self.logger.log(ERR, "Maven build failed")
                        status = 402
            else:
                logger.log(ERR, "Build result file not found at " + self.buildLogFile + " as espected")
                status = 404
        elif self.isCompiled():
            self.logger.log(INFO, "Compiling succeed")
            status = 200
        else:
            self.logger.log(ERR, "Compiling failed : 402")
            status = 402


        return status
    
    
class Judge:
    def __init__(self, workingDir, testcase,  language, timeout, dirty, logger):
        self.logger = logger
        self.workingDirectory = workingDir
        self.language = language
        self.timeout = timeout
        self.dirty = dirty
        self.testCase = testcase
        
        self.submitedFile = workingDir + os.sep + MAIN_FILE_NAME
        self.inputsfile =  workingDir + os.sep + testcase + os.sep + INPUT_FILE_NAME
        self.espectedOutputsfile =  workingDir + os.sep + testcase + os.sep + OUTPUT_FILE_NAME
        self.responseLogFile =  workingDir + os.sep + testcase + os.sep + RUN_RESPONSE_FILE_NAME
    
    def getExecutionFileExtension(self):
        if self.language == "java":
            return ".java"
        elif self.language in ("cpp", "c", "make"):
            return ".out"
        elif self.language == "python":
            return ".py"
    
    def mainExecutionFileExists(self):
        mainFileHere = False
        for f in os.listdir(self.workingDirectory):
            if MAIN_FILE_NAME + self.getExecutionFileExtension() == f:
                mainFileHere = True
        return mainFileHere
    
    def run(self):
        
        if not self.mainExecutionFileExists():
            self.logger.log(ERR, "No main file to execute found")
            return 404
        
        print("continue running")
        
        # Build timout commandline
        syst = platform.system()
        timeoutcmd = ''
        if syst == "Darwin":
            timeoutcmd = "gtimeout " + self.timeout
        elif syst == "Windows":
            timeoutcmd = ""
        else:
            timeoutcmd =  "timeout " + self.timeout

        # Build the run command line
        fileToExecute = self.submitedFile + self.getExecutionFileExtension()
        
        runCmd = ""
        if self.language in ('c', 'cpp', 'make'):
            runCmd = fileToExecute
        elif self.language == 'java':
            runCmd = "java -cp " + self.workingDirectory + " " + MAIN_FILE_NAME
        else:
            runCmd = self.language + " " + fileToExecute
        
        # Building the command line that will be run
        commandToRun = timeoutcmd + ' ' + runCmd + ' < ' + self.inputsfile + ' > ' + self.responseLogFile
        
        self.logger.log(INFO, "Running code with command: " + commandToRun)
        
        run_return_code = os.system(commandToRun)
        
        self.logger.log(INFO, "Run finished...")
        
        # Clean the directory if needed
        if not self.dirty:
            self.logger.log(INFO, "Cleaning working directory...")
            delCmd = ""
            if(platform.system() == "Windows"):
              delCmd = "del "
            else:
                delCmd = "rm "
            if self.language in ("c", "cpp"):
                os.system(delCmd + self.workingDirectory + os.sep + "*.out");
            elif self.language == "java":
                os.system(delCmd + self.workingDirectory + os.sep + "*.class");
        
        # Return the status code
        status = 200
        if run_return_code == 0: # OK
            self.logger.log(INFO, "Run succeed")
            status = 200
        elif run_return_code == 31744: # TIMEOUT
            self.logger.log(INFO, "Timeout limit reached : 408")
            status = 408
        else:
            self.logger.log(ERR, "Execution failed : 400")
            status = 400
    
        return status
    
    def match(self):
        self.logger.log(INFO, "Matching datas from " + self.responseLogFile + " with espected outputs from " + self.espectedOutputsfile + "...")
        if os.path.isfile(self.responseLogFile) and os.path.isfile(self.espectedOutputsfile):
            if not filecmp.cmp(self.responseLogFile, self.espectedOutputsfile):
                self.logger.log(INFO, "Datas does not match: 401: WRONG ANSWER")
                return 401
            self.logger.log(INFO, "Datas matched: 200: SUCCESS")
            return 200
        self.logger.log(ERR, "File not found: 404")
        return 404

    def verdict(self):
        return_code = self.run()
        if return_code == 200:
            return_code = self.match()
        return return_code

def main(argv):
    workingdir = ''
    language = ''
    timeout = 1
    verbose = False
    dirty = False
    includes = ""
    tests = ""
    print("Running judge...")
	
    try:
        opts, args = getopt.getopt(argv,"hvdl:t:i:c:",["help", "verbose", "dirty", "language=", "timeout=", "includes=", "testscases="])
    except getopt.GetoptError as err:
        print('judge.py -verbose -dirty  -l <language> -t <timeout> -i <includes> -c <testCases>  TARGET')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', "--help"):
            print('judge.py -verbose -dirty  -l <language> -t <timeout> -i <includes> -c <testCases> TARGET')
            sys.exit()
        elif opt in ("-l", "--language"):
            language = arg
        elif opt in ("-t", "--timeout"):
            timeout = arg
        elif opt in ("-v", "--verbose"):
            verbose = True
        elif opt in ("-d", "--dirty"):
            dirty = True
        elif opt in ("-i", "--includes"):
            includes = arg
        elif opt in ("-c", "--testcases"):
            tests = arg.split(',')
        else:
            assert False, "unhandled option"
    
    if len(args) <= 0:
        print("Error, no target given")
        sys.exit(2)

    workingdir = args[0]

    if not workingDirCorrect(workingdir, language):
        print("Error : Invalid working directory")
        sys.exit(2)
        
    logger = MyLogger(workingdir, verbose)
    
    if not language in SUPPORTED_LANGUAGES:
        logger.log(ERR, "Language not supported")
        sys.exit(2)
    
    status = 200
    
    if language in COMPILED_LANGUAGES:
        compiler = Compiler(workingdir, language, includes, logger)
        status = compiler.compileSources()
        with open(workingdir + os.sep + BUILD_STATUS_NAME, "a") as myfile:
            myfile.write(str(status))
    
    if status == 200 and language != 'maven':
        testCases = getTestCases(workingdir, tests)
        
        logger.log(INFO, str(len(testCases)) + " testcases found")

        for i, testCase in enumerate(testCases, start=1):
            logger.log(INFO, "Running judge for testcase " + testCase + " : " + str(i) + "/" + str(len(testCases))) 
            
            judge = Judge(workingdir, testCase, language, timeout, dirty, logger)
            status = judge.verdict()
            
            with open(workingdir + os.sep + testCase + os.sep + VERDICT_FILE_NAME, "a") as verdictfile:
                verdictfile.write(str(status))
    
    logger.log(INFO, "Job finished")
    
    return status


if __name__ == "__main__":
   main(sys.argv[1:])
