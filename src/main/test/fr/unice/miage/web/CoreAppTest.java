package fr.unice.miage.web;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.common.io.Files;

import fr.unice.miage.web.controllers.CoreJudgeController;

@WebAppConfiguration
@ContextConfiguration(classes = WebConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class CoreAppTest {
	
	private final static String TESTS_FILE = System.getProperty("user.dir")+File.separator+"src"+File.separator+"main"+File.separator+"test-resourses"+File.separator+"56fc3a8b353e1";
	
	private MockMvc mvc;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(new CoreJudgeController()).build();
	}
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Test
	public void test() throws Exception {
		
		byte[] f1 = Files.toByteArray(new File(TESTS_FILE + File.separator + "file"));
		byte[] f2 = Files.toByteArray(new File(TESTS_FILE + File.separator + "tests"));
		
		assert(f1.length > 0);
		assert(f2.length > 0);
		
		MockMultipartFile file = new MockMultipartFile("file", f1);
		MockMultipartFile tests = new MockMultipartFile("tests", f2);

		mvc.perform(MockMvcRequestBuilders.fileUpload("/CoreJudge/submitJob")
        		.file(file)
				.file(tests)
				.param("language", "pyhton")
				.param("timeout", "3")
				.param("ref", "t" + System.currentTimeMillis()))
			.andExpect(status().isOk());
		
		System.out.println("CONTENT = " + content());
	}

}
